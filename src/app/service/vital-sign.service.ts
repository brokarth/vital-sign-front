import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";

import { Worker } from "../model/worker";
import { Location } from "../model/location";
import { OxygenSaturation } from "../model/oxygen-saturation";

@Injectable()
export class VitalSignService {
  readonly serviceUrl = "http://localhost:8080/vital-sign/";

  constructor(private http: HttpClient) {}

  public getLastHourVitalSign(): Observable<OxygenSaturation[]> {
    // return this.mockData();
    return this.http.get<OxygenSaturation[]>(`${this.serviceUrl}`);
  }

  public getLastHourVitalSignByWorker(
    worker: Worker
  ): Observable<OxygenSaturation[]> {
    return this.http.get<OxygenSaturation[]>(
      `${this.serviceUrl}/${worker.rut}`
    );
  }
}
