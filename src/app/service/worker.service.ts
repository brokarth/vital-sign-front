import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";

import { Worker } from "../model/worker";

@Injectable()
export class WorkerService {
  readonly serviceUrl = "http://localhost:8080/worker";

  constructor(private http: HttpClient) {}

  public getByName(name: string): Observable<Worker[]> {
    return this.http.get<Worker[]>(`${this.serviceUrl}/by-name/${name}`);
  }

  public getByRut(workerRut: number): Observable<Worker> {
    return this.http.get<Worker>(`${this.serviceUrl}/by-rut/${workerRut}`);
  }

  public mockSingleData() {
    return of(new Worker(11111111, "Test1"));
  }
}
