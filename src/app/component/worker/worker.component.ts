import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import { Worker } from "../../model/worker";
import { VitalSignService } from "../../service/vital-sign.service";
import { OxygenSaturation } from "../../model/oxygen-saturation";
import { WorkerService } from "../../service/worker.service";
@Component({
  templateUrl: "./worker.html"
})
export class WorkerComponent implements OnInit {
  private worker: Worker;
  private oxygenVitalSigns: Observable<OxygenSaturation[]>;

  constructor(
    private vitalSignService: VitalSignService,
    private workerService: WorkerService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit() {
    const rut = +this.route.snapshot.paramMap.get("rut");
    this.workerService.getByRut(rut).subscribe(worker => {
      this.worker = worker;
      this.oxygenVitalSigns = this.vitalSignService.getLastHourVitalSignByWorker(
        worker
      );
    });
  }

  getCreationDate(stringDate: string): string {
    const date = new Date(stringDate);
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];

    const day = date.getDate();
    const monthName = monthNames[date.getMonth()];
    const year = date.getFullYear();
    const hour = date.getHours();
    const minutes = date.getMinutes();

    return `${day}/${monthName}/${year} ${hour}:${minutes}`;
  }
}
