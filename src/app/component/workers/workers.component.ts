import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/switchMap";

import { WorkerService } from "../../service/worker.service";
import { Worker } from "../../model/worker";
@Component({
  templateUrl: "./workers.html"
})
export class WorkersComponent implements OnInit {
  private workers: Observable<Worker[]>;
  private workerNameStream = new Subject<string>();

  constructor(private workService: WorkerService) {}

  search(workerName: string) {
    this.workerNameStream.next(workerName);
  }

  ngOnInit() {
    this.workers = this.workerNameStream
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap((workerName: string) =>
        this.workService.getByName(workerName)
      );
  }
}
