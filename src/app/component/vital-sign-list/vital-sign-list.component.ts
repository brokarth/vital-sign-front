import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { OxygenSaturation } from "../../model/oxygen-saturation";
import { VitalSignService } from "../../service/vital-sign.service";

@Component({
  selector: "vital-sign-list",
  templateUrl: "./vital-sign-list.html"
})
export class VitalSignListComponent implements OnInit {
  private readonly title = "Listado de signos vitales";
  private oxygenVitalSigns: Observable<OxygenSaturation[]>;

  constructor(private vitalSignService: VitalSignService) {}

  ngOnInit(): void {
    this.oxygenVitalSigns = this.vitalSignService.getLastHourVitalSign();
  }

  getCreationDate(stringDate: string): string {
    const date = new Date(stringDate);
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];

    const day = date.getDate();
    const monthName = monthNames[date.getMonth()];
    const year = date.getFullYear();
    const hour = date.getHours();
    const minutes = date.getMinutes();

    return `${day}/${monthName}/${year} ${hour}:${minutes}`;
  }
}
