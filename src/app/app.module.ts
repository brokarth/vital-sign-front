import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import {
  MatCardModule,
  MatExpansionModule,
  MatDividerModule,
  MatSidenavModule,
  MatListModule,
  MatFormFieldModule,
  MatInputModule
} from "@angular/material";

import { AppComponent } from "./app.component";
import { VitalSignListComponent } from "./component/vital-sign-list/vital-sign-list.component";
import { VitalSignService } from "./service/vital-sign.service";
import { WorkerService } from "./service/worker.service";
import { AppRoutingModule } from "./app-routing.module";
import { WorkersComponent } from "./component/workers/workers.component";
import { WorkerComponent } from "./component/worker/worker.component";

@NgModule({
  declarations: [
    AppComponent,
    VitalSignListComponent,
    WorkersComponent,
    WorkerComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    HttpClientModule,
    MatCardModule,
    MatExpansionModule,
    MatDividerModule,
    MatSidenavModule,
    MatListModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule
  ],
  providers: [VitalSignService, WorkerService],
  bootstrap: [AppComponent]
})
export class AppModule {}
