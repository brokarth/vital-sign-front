import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { WorkersComponent } from "./component/workers/workers.component";
import { VitalSignListComponent } from "./component/vital-sign-list/vital-sign-list.component";
import { WorkerComponent } from "./component/worker/worker.component";

const routes: Routes = [
  { path: "", redirectTo: "/vital-signs", pathMatch: "full" },
  { path: "vital-signs", component: VitalSignListComponent },
  { path: "workers", component: WorkersComponent },
  { path: "worker/:rut", component: WorkerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
