import { Worker } from "./worker";
import { Location } from "./location";

export class OxygenSaturation {
  constructor(
    private creationTime: string,
    private oxygenSaturationValue: number,
    private worker: Worker,
    private location: Location
  ) {}
}
