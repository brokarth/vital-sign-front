import { TestBed, inject } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { of } from "rxjs/observable/of";

import { WorkerService } from "../../app/service/worker.service";
import { Worker } from "../../app/model/worker";

describe("WorkerService", () => {
  let httpClientSpy: { get: jasmine.Spy };
  let workerService: WorkerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WorkerService]
    });

    httpClientSpy = jasmine.createSpyObj("HttpClient", ["get"]);
    workerService = new WorkerService(<any>httpClientSpy);
  });

  it(
    "should be created",
    inject([WorkerService], (service: WorkerService) => {
      expect(service).toBeTruthy();
    })
  );

  it("should get workers by name", () => {
    const workerName = "Test";
    const workers = [
      new Worker(11111111, `${workerName}1`),
      new Worker(22222222, `${workerName}2`),
      new Worker(33333333, `${workerName}3`)
    ];

    httpClientSpy.get.and.returnValue(of(workers));

    workerService.getByName(workerName).subscribe(workers => {
      expect(workers).toBeTruthy();
      expect(workers.length).toEqual(3);
      expect(workers[0].name).toContain(name);
    });

    expect(httpClientSpy.get.calls.count()).toBe(1, "one call");
  });

  it("should get worker by rut", () => {
    const workerRut = 11111111;
    const expectedWorker = new Worker(11111111, `Test`);

    httpClientSpy.get.and.returnValue(of(expectedWorker));

    workerService.getByRut(workerRut).subscribe(worker => {
      expect(worker).toBeTruthy();
      expect(worker.name).toEqual(expectedWorker.name);
      expect(worker.rut).toEqual(expectedWorker.rut);
    });

    expect(httpClientSpy.get.calls.count()).toBe(1, "one call");
  });
});
