import { TestBed, inject } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { of } from "rxjs/observable/of";

import { VitalSignService } from "../../app/service/vital-sign.service";
import { Worker } from "../../app/model/worker";
import { Location } from "../../app/model/location";
import { OxygenSaturation } from "../../app/model/oxygen-saturation";

describe("VitalSignService", () => {
  let httpClientSpy: { get: jasmine.Spy };
  let vitalSignService: VitalSignService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VitalSignService]
    });

    httpClientSpy = jasmine.createSpyObj("HttpClient", ["get"]);
    vitalSignService = new VitalSignService(<any>httpClientSpy);
  });

  it(
    "should be created",
    inject([VitalSignService], (service: VitalSignService) => {
      expect(service).toBeTruthy();
    })
  );

  it("should get last hour vital signs", () => {
    const worker = new Worker(11111111, "Test");
    const location = new Location(0, 0);
    const lastHourVitalSigns = [
      new OxygenSaturation(new Date(), 98, worker, location),
      new OxygenSaturation(new Date(), 99, worker, location),
      new OxygenSaturation(new Date(), 100, worker, location)
    ];

    httpClientSpy.get.and.returnValue(of(lastHourVitalSigns));

    vitalSignService.getLastHourVitalSign().subscribe(vitalSigns => {
      expect(vitalSigns).toBeTruthy();
      expect(vitalSigns.length).toEqual(3);
    });

    expect(httpClientSpy.get.calls.count()).toBe(1, "one call");
  });

  it("should get last hour vital signs by worker", () => {
    const worker = new Worker(11111111, "Test");
    const location = new Location(0, 0);
    const lastHourVitalSigns = [
      new OxygenSaturation(new Date(), 98, worker, location),
      new OxygenSaturation(new Date(), 99, worker, location),
      new OxygenSaturation(new Date(), 100, worker, location)
    ];

    httpClientSpy.get.and.returnValue(of(lastHourVitalSigns));

    vitalSignService
      .getLastHourVitalSignByWorker(worker)
      .subscribe(vitalSigns => {
        expect(vitalSigns).toBeTruthy();
        expect(vitalSigns.length).toEqual(3);
      });

    expect(httpClientSpy.get.calls.count()).toBe(1, "one call");
  });
});
